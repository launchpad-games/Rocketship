# Rocketship 

A DiscordJS v14 bot for the [Launchpad Games Discord server](https://discord.gg/qP9nUpGWUu)

# Packages used
- Discord.js v14
- dotenv 
- jsonfile
- mongoose (MongoDB)

# Deploy
```
$ cd Desktop
$ git clone https://gitlab.com/LaunchpadGames/Rocketship
$ cd Rocketship
-> create a file called ".env" based off ".env-example.txt"
$ npm install
$ node index.js
```

to start the bot with your computer automatically on boot (on linux systems):
```
-> copy the contents of "rocketship.service"
$ sudo vim /etc/systemd/system/rocketship.service
    # you can use nano instead of vim if you like 
-> paste what you copied and change the WorkingDirectory and User fields to match your configuration
$ sudo systemctl daemon-reload
$ sudo systemctl start rocketship
$ sudo systemctl enable rocketship
```
